<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class C_admin extends Controller
{
    function index()
    {
        return view('admin.dashboard');
    }

    function home()
    {
        return view('admin.home');
    }
}


@extends('layouts.home')

@section('content')
<section id="about-us" class="cta">
    <div class="container">
      <div class="d-flex justify-content-center align-items-end" style="height: 150px;">
          <div class="text-center">
            <h3>Klien Kami</h3>
            <p>Daftar Klien yang bekerja sama dengan komunitas kami</p>
          </div>
      </div>
    </div>
  </section>
  <section id="artikel">
    <div class="container">
        <h2>Data Klien Produk Spratpart Supra</h2>
        <table class="table table-bordered">
            <thead class="thead-dark">
              <tr>
                <th>ID</th>
                <th>Nama Klien</th>
                <th>Testimony</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>John Doe</td>
                <td>"Produk Supra Spare Part di KMS sangat membantu memperbaiki motor saya. Kualitasnya luar biasa!"</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jane Smith</td>
                <td>"Saya sangat puas dengan Supra Spare Part di KMS. Harganya terjangkau dan kualitasnya terbaik."</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Alice Johnson</td>
                <td>"Supra Spare Part di KMS benar-benar memenuhi ekspektasi saya. Performanya sangat baik!"</td>
              </tr>
            </tbody>
          </table>
      </div>
  </section>
@endsection

@extends('layouts.auth')

@section('content')
<div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
    <div class="login-brand">
        <img src="https://i.pinimg.com/736x/30/6a/f0/306af0cceb1b483fd4b2be47ab14d81e.jpg" alt="logo" width="100" class="shadow-light rounded-circle">
    </div>
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
    <div class="card card-primary">
        <div class="card-header">
            <h4>Login</h4>
        </div>


        <div class="card-body">
            <form method="POST" action="{{ url('/auth') }}" class="needs-validation" novalidate="">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" tabindex="1" required autofocus>
                    <div class="invalid-feedback">
                        Please fill in your email
                    </div>
                </div>
                    @csrf
                <div class="form-group">
                    <div class="d-block">
                        <label for="password" class="control-label">Password</label>
                    </div>
                    <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                    <div class="invalid-feedback">
                        please fill in your password
                    </div>
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                        Login
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
